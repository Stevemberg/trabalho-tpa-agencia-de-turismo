package controle;

import agencia.Pacote;
import agencia.Passeio;
import agencia.Viagem;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

public class Controlador {
    
    private final HashMap<String,Pacote> pacotes = new HashMap();
    private final HashMap<String,Passeio> passeios = new HashMap();
    private final HashMap<String,Viagem> viagens = new HashMap();
    
    public int IncluirPacote(String codigo, float precoInicial, String cidade, String hotel, int dias){
        
        Pacote p = new Pacote(codigo, precoInicial,cidade,hotel,dias);
        
        if(pacotes.get(p.getCodigo()) == null){
            pacotes.put(p.getCodigo(), p);       
            return 1;
        }
        
        return 0;
        
    }
    
    public int AlterarPacote(String codigo, float precoInicial, String cidade, String hotel, int dias){
        
        Pacote p = pacotes.get(codigo);
        
        if(p == null){
            return 0;
        }else{
            p.setPrecoInicial(precoInicial);
            p.setHotel(hotel);
            p.setCidade(cidade);
            p.setDias(dias);
            return 1;
        }
        
    }
    
    public int AlterarPorcentagem(float porcentagem){
        
        if(pacotes.isEmpty()){
            return 0;
        }
        
        for(String codigo : pacotes.keySet()){
            Pacote p = pacotes.get(codigo);
            p.porcentagem = porcentagem;
        }
        
        return 1;
        
    }
    
    public int ExcluirPacote(String codigo){
        
        Pacote p = pacotes.get(codigo);
        
        if(p == null){
            return 0;
        }else{
            pacotes.remove(codigo);
            return 1;
        }
        
    }
    
    public int IncluirPasseio(String codigo, float preco, String cidadePasseio, String lugar, String tipo, String horaInicio, String horaFim){
        
        int hi,hf,mi,mf;
        
        hi = Integer.parseInt(String.valueOf(horaInicio.charAt(0)) + String.valueOf(horaInicio.charAt(1)));
        hf = Integer.parseInt(String.valueOf(horaFim.charAt(0)) + String.valueOf(horaFim.charAt(1)));
        mi = Integer.parseInt(String.valueOf(horaInicio.charAt(3)) + String.valueOf(horaInicio.charAt(4)));
        mf = Integer.parseInt(String.valueOf(horaFim.charAt(3)) + String.valueOf(horaFim.charAt(4)));
                        
        if(hi>hf){
            return 0;
        }else{
            if(hi==hf){
                if(mi>=mf){
                    return 0;
                }
            }
        }
        
        Passeio p = new Passeio(codigo,preco,cidadePasseio,lugar,tipo,horaInicio,horaFim);
        
        if(passeios.get(p.getCodigo()) == null){
            passeios.put(p.getCodigo(), p);
            return 1;
        }
        
        return 0;
        
    }
    
    public int AlterarPasseio(String codigo, float preco, String cidadePasseio, String lugar, String tipo, String horaInicio, String horaFim){
        
        int hi,hf,mi,mf;
        
        hi = Integer.parseInt(String.valueOf(horaInicio.charAt(0)) + String.valueOf(horaInicio.charAt(1)));
        hf = Integer.parseInt(String.valueOf(horaFim.charAt(0)) + String.valueOf(horaFim.charAt(1)));
        mi = Integer.parseInt(String.valueOf(horaInicio.charAt(3)) + String.valueOf(horaInicio.charAt(4)));
        mf = Integer.parseInt(String.valueOf(horaFim.charAt(3)) + String.valueOf(horaFim.charAt(4)));
        
        if(hi>hf){
            return 0;
        }else{
            if(hi==hf){
                if(mi>=mf){
                    return 0;
                }
            }
        }
 
        Passeio p = passeios.get(codigo);
        
        if(p == null){
            return 0;
        }else{
            p.setPreco(preco);
            p.setCidadePasseio(cidadePasseio);
            p.setLugar(lugar);
            p.setTipo(tipo);
            p.setHoraFim(horaFim);
            p.setHoraInicio(horaInicio);
            return 1;
        }
        
    }
    
    public int ExcluirPasseio(String codigo){
        
        Passeio p = passeios.get(codigo);
        
        if(p == null){
            return 0;
        }else{
            passeios.remove(codigo);
            return 1;
        }
        
    }

    public int IncluirViagem(String codigo, String dataInicio, String codigoPacote, ArrayList codigosPasseios){
        
        Pacote p = pacotes.get(codigoPacote);
        ArrayList<Passeio> passeiosViagem = new ArrayList();
        
        int conta=0;
        
        if(p==null){
            return 0;
        }
        
        if(codigosPasseios!=null){
                
                for(Object cp : codigosPasseios){

                    Passeio pa = passeios.get(String.valueOf(cp));
                    
                    if(pa.getCidadePasseio().equals(p.getCidade())){
                        conta++;
                    }

                }
         
            if(conta!=codigosPasseios.size()){
                return 0;
            }
            
            for(Object cp :codigosPasseios){
                Passeio passeio = passeios.get(String.valueOf(cp));
                passeiosViagem.add(passeio);
            }
            
        }else{
            passeiosViagem = null;
        }

        
        Viagem v = new Viagem(codigo,dataInicio,p);
        
        
        v.setPasseios(passeiosViagem);
        v.setPrecoFinal();
        viagens.put(v.getCodigo(), v);
        
        return 1;
        
    }

    public int AlterarViagem(String codigo, String dataInicio, String codigoPacote, ArrayList codigosPasseios){
        
        Pacote p = pacotes.get(codigoPacote);
        ArrayList<Passeio> passeiosViagem = new ArrayList();
        
        int conta=0;
        
        if(p==null){
            return 0;
        }
        
        if(codigosPasseios!=null){
                
                for(Object cp : codigosPasseios){

                    Passeio pa = passeios.get(String.valueOf(cp));
                    
                    if(pa.getCidadePasseio().equals(p.getCidade())){
                        conta++;
                    }

                }
         
            if(conta!=codigosPasseios.size()){
                return 0;
            }
            
            for(Object cp :codigosPasseios){
                Passeio passeio = passeios.get(String.valueOf(cp));
                passeiosViagem.add(passeio);
            }
            
        }else{
            passeiosViagem = null;
        }

        Viagem v = viagens.get(codigo);
        v.setPacote(p);
        v.setDataInicio(dataInicio);
        v.setPasseios(passeiosViagem);
        v.setPrecoFinal();

        return 1;
        
    }
    
    public int ExcluirViagem(String codigo){
        
        Viagem v = viagens.get(codigo);
        
        if(v==null){
            return 0;
        }else{
            viagens.remove(codigo);
            return 1;
        }
    }

    public void relatorioAnual(int ano) {
        
        float precoTotal=0, precoFuturo=0;
        
        Calendar cal = GregorianCalendar.getInstance();
        int anoatual = cal.get(Calendar.YEAR);
        int mesatual = 1 + cal.get(Calendar.MONTH);
        int diaatual = cal.get(Calendar.DAY_OF_MONTH);
        
        Viagem v=null;
        if(ano<anoatual){
        
        for(String codigo : viagens.keySet()){
            v = viagens.get(codigo);
            String anoviagem = String.valueOf(v.getDataInicio().charAt(6))+String.valueOf(v.getDataInicio().charAt(7))+
                    String.valueOf(v.getDataInicio().charAt(8))+String.valueOf(v.getDataInicio().charAt(9));
            if(Integer.parseInt(anoviagem)==ano){
                precoTotal = precoTotal + v.getPrecoFinal();
                System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                System.out.print("\nData da Viagem: " + v.getDataInicio());
                System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                System.out.print("\n");
            }       
        }
        
        if(v!=null){
            System.out.print("\nValor arrecadado no ano de " + ano + ": " + precoTotal);
            System.out.println("\n");
        }else{
            System.out.println("\nNão há viagens registradas nesse mês.");
        }
        }else{
            if(ano==anoatual){
                for(String codigo : viagens.keySet()){
                    v = viagens.get(codigo);
                    String anoviagem = String.valueOf(v.getDataInicio().charAt(6))+String.valueOf(v.getDataInicio().charAt(7))+
                    String.valueOf(v.getDataInicio().charAt(8))+String.valueOf(v.getDataInicio().charAt(9));
                    String mesviagem = String.valueOf(v.getDataInicio().charAt(3))+String.valueOf(v.getDataInicio().charAt(4));
                    String diaviagem = String.valueOf(v.getDataInicio().charAt(0))+String.valueOf(v.getDataInicio().charAt(1));
                    if(Integer.parseInt(anoviagem)==ano){
                        if(Integer.parseInt(mesviagem)<mesatual){
                            precoTotal = precoTotal + v.getPrecoFinal(); 
                            System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                            System.out.print("\nData da Viagem: " + v.getDataInicio());
                            System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                            System.out.print("\n");
                        }else{
                            if(Integer.parseInt(mesviagem)==mesatual){
                                if(Integer.parseInt(diaviagem)<=diaatual){
                                    precoTotal = precoTotal + v.getPrecoFinal(); 
                                    System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                                    System.out.print("\nData da Viagem: " + v.getDataInicio());
                                    System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                                    System.out.print("\n");
                                }else{
                                    precoFuturo = precoFuturo + v.getPrecoFinal();
                                    System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                                    System.out.print("\nData da Viagem: " + v.getDataInicio());
                                    System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                                    System.out.print("\n");
                                }
                            }else{
                                precoFuturo = precoFuturo + v.getPrecoFinal();
                                System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                                System.out.print("\nData da Viagem: " + v.getDataInicio());
                                System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                                System.out.print("\n");
                            }
                        }
                    }       
                }
        
                if(v!=null){
                System.out.print("\nValor arrecadado no ano de " + ano + ": " + precoTotal);
                System.out.print("\nValor ainda a ser arrecadado no ano de " + ano + ": " + precoFuturo);
                System.out.println("\n");
                }else{
                    System.out.println("\nNão há viagens registradas nesse mês.");
                }
            }else{
                for(String codigo : viagens.keySet()){
                    v = viagens.get(codigo);
                    String anoviagem = String.valueOf(v.getDataInicio().charAt(6))+String.valueOf(v.getDataInicio().charAt(7))+
                    String.valueOf(v.getDataInicio().charAt(8))+String.valueOf(v.getDataInicio().charAt(9));
                    if(Integer.parseInt(anoviagem)==ano){
                        precoFuturo = precoFuturo + v.getPrecoFinal();
                        System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                        System.out.print("\nData da Viagem: " + v.getDataInicio());
                        System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                        System.out.print("\n");
                    }       
                }
                if(v!=null){
                    System.out.print("\nValor a ser arrecadado no ano de " + ano + ": " + precoFuturo);
                    System.out.println("\n");
                }else{
                    System.out.println("\nNão há viagens registradas nesse mês.");
                }
            }
        }
        
    }
    
public void relatorioMensal(int mes) {
        
    float precoTotal=0, precoFuturo=0;
        
    Calendar cal = GregorianCalendar.getInstance();
    int anoatual = cal.get(Calendar.YEAR);
    int mesatual = 1 + cal.get(Calendar.MONTH);
    int diaatual = cal.get(Calendar.DAY_OF_MONTH);
     
    Viagem v=null;
    for(String codigo : viagens.keySet()){
        v = viagens.get(codigo);
        String mesviagem = String.valueOf(v.getDataInicio().charAt(3))+String.valueOf(v.getDataInicio().charAt(4));
        String anoviagem = String.valueOf(v.getDataInicio().charAt(6))+String.valueOf(v.getDataInicio().charAt(7))+
        String.valueOf(v.getDataInicio().charAt(8))+String.valueOf(v.getDataInicio().charAt(9));
        String diaviagem = String.valueOf(v.getDataInicio().charAt(0))+String.valueOf(v.getDataInicio().charAt(1));
        if(Integer.parseInt(anoviagem)==anoatual){
            if(Integer.parseInt(mesviagem)==mes){
                System.out.print("\nCódigo da Viagem: " + v.getCodigo());
                System.out.print("\nData da Viagem: " + v.getDataInicio());
                System.out.print("\nPreço da Viagem: " + v.getPrecoFinal());
                System.out.print("\n");
                if(mes==mesatual){
                    if(Integer.parseInt(diaviagem)<=diaatual){
                       precoTotal = precoTotal + v.getPrecoFinal();
                        }else{
                            precoFuturo = precoFuturo + v.getPrecoFinal();
                        }
                    }else{
                        precoTotal = precoTotal + v.getPrecoFinal();
                    }
                }
            }
        }
        
        if(v!=null){
            System.out.print("\nValor arrecadado no mês " + mes + ": " + precoTotal);
            System.out.print("\nValor ainda a ser arrecadado no mês " + mes + ": " + precoFuturo);
            System.out.println("\n");
        }else{
            System.out.println("\nNão há viagens registradas nesse mês.");
        }

}
        
}
