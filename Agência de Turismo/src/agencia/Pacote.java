package agencia;

public class Pacote {
    
    private float precoInicial;
    private String cidade;
    private String hotel;
    private int dias;
    public float porcentagem=0;
    private final String codigo;

    public Pacote(String co, float p, String c, String h, int d) {
        this.precoInicial = p;
        this.cidade = c;
        this.hotel = h;
        this.dias = d;
        this.codigo = co;
    }

    public float getPrecoInicial() {
        return precoInicial;
    }

    public void setPrecoInicial(float p) {
        this.precoInicial = p;
    }

    public String getCidade() {
        return cidade;
    }

    public void setCidade(String c) {
        this.cidade = c;
    }

    public String getHotel() {
        return hotel;
    }

    public void setHotel(String h) {
        this.hotel = h;
    }

    public int getDias() {
        return dias;
    }

    public void setDias(int d) {
        this.dias = d;
    }

    public float getPorcentagem() {
        return porcentagem;
    }

    public void setPorcentagem(float po) {
        this.porcentagem = po;
    }
    
    public String getCodigo(){
        return codigo;
    }

}
