package agencia;

public class Passeio {
    
    private float preco;
    private String cidadePasseio;
    private String lugar;
    private String tipo;
    private String horaInicio;
    private String horaFim;
    private final String codigo;

    public Passeio(String co, float p, String c, String l, String t, String hi, String hf) {
        this.preco = p;
        this.cidadePasseio = c;
        this.lugar = l;
        this.tipo = t;
        this.horaInicio = hi;
        this.horaFim = hf;
        this.codigo = co;
    }

    public float getPreco() {
        return preco;
    }

    public void setPreco(float p) {
        this.preco = p;
    }

    public String getCidadePasseio() {
        return cidadePasseio;
    }

    public void setCidadePasseio(String c) {
        this.cidadePasseio = c;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String l) {
        this.lugar = l;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String t) {
        this.tipo = t;
    }

    public String getHoraInicio() {
        return horaInicio;
    }

    public void setHoraInicio(String hi) {
        this.horaInicio = hi;
    }

    public String getHoraFim() {
        return horaFim;
    }

    public void setHoraFim(String hf) {
        this.horaFim = hf;
    }
    
    public String getCodigo(){
        return codigo;
    }
    
}
