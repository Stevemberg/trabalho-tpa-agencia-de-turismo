package agencia;

import java.util.ArrayList;

public class Viagem {
    
    private float precoFinal;
    private String dataInicio;
    private ArrayList<Passeio> passeios = new ArrayList();
    private Pacote pacote;
    private final String codigo;

    public Viagem(String co, String d, Pacote p) {
        this.dataInicio = d;
        this.pacote = p;
        this.codigo = co;
    }

    public float getPrecoFinal() {
        return precoFinal;
    }

    public void setPrecoFinal() {
        
        float pf,precoPasseios=0;
        
        if(passeios!=null){
            for(Passeio p: passeios){
                precoPasseios = precoPasseios + p.getPreco();
            }
        }else{
            precoPasseios = 0;
        }
        pf = pacote.getPrecoInicial() + precoPasseios;
        this.precoFinal = pf;
    }

    public String getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(String d) {
        this.dataInicio = d;
    }

    public ArrayList<Passeio> getPasseios() {
        return passeios;
    }

    public void setPasseios(ArrayList<Passeio> pa) {
        this.passeios = pa;
    }
    
    public Pacote getPacote() {
        return pacote;
    }

    public void setPacote(Pacote p) {
        this.pacote = p;
    }

    public String getCodigo(){
        return codigo;
    }
}
