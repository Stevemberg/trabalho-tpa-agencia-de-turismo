package visualizacao;

import controle.Controlador;
import java.util.ArrayList;
import java.util.Scanner;

public class AgênciaDeTurismo {

    private final Controlador ctrl = new Controlador();
    Scanner sc = new Scanner(System.in);
    static AgênciaDeTurismo at = new AgênciaDeTurismo();
    
    public static void main(String[] args) {
        at.menu();
    }
    
    public void menu(){
        int opcao;
        
        System.out.println("Agência de Turismo");
        
        do{
            System.out.println("1 - Gerenciar Pacotes");
            System.out.println("2 - Gerenciar Passeios");
            System.out.println("3 - Gerenciar Viagens");
            System.out.println("4 - Visualizar Relatório");
            System.out.println("5 - Sair");
            opcao = Integer.parseInt(sc.nextLine());
            switch(opcao){
                case 1:
                    at.menu2();
                break;
                case 2:
                    at.menu3();
                break;
                case 3:
                    at.menu4();
                break;
                case 4:
                    at.menu5();
                break;
            }
        }while(opcao != 5);
        
    }
    
public void menu2(){      
        int opcao;
        
        System.out.println("Gerenciar Pacotes");
        
        do{
            System.out.println("1 - Incluir");
            System.out.println("2 - Alterar");
            System.out.println("3 - Alterar Porcentagem de pagamento");
            System.out.println("4 - Excluir");
            System.out.println("5 - Sair");
            opcao = Integer.parseInt(sc.nextLine());
            switch(opcao){
                case 1:
                   at.IncluirPacote();
                break;
                case 2:
                    at.AlterarPacote();
                break;
                case 3:
                    at.AlterarPorcentagem();
                break;
                case 4:
                    at.ExcluirPacote();
                break;
            }
        }while(opcao != 5);
        
    }


private void IncluirPacote(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    System.out.println("Preço:");
    float precoInicial = Float.parseFloat(sc.nextLine());
    
    System.out.println("Cidade:");
    String cidade = sc.nextLine();
    
    System.out.println("Hotel:");
    String hotel = sc.nextLine();
    
    System.out.println("Número de dias:");
    int dias = Integer.parseInt(sc.nextLine());
    
    int res = ctrl.IncluirPacote(codigo, precoInicial, cidade, hotel, dias);
    
    if(res==1){
        System.out.println("Pacote incluído com sucesso!");
    }else{
        System.out.println("Falha no cadastro do pacote.");
    }
   
}

private void AlterarPacote(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    System.out.println("Novo preço:");
    float precoInicial = Float.parseFloat(sc.nextLine());
    
    System.out.println("Nova cidade:");
    String cidade = sc.nextLine();
    
    System.out.println("Novo Hotel:");
    String hotel = sc.nextLine();
    
    System.out.println("Novo número de dias:");
    int dias = Integer.parseInt(sc.nextLine());
    
    int res = ctrl.AlterarPacote(codigo,precoInicial, cidade, hotel, dias);
    
    if(res==1){
        System.out.println("Pacote alterado com sucesso!");
    }else{
        System.out.println("Falha na alteração do pacote.");
    }
    
}

private void AlterarPorcentagem(){
    
    System.out.println("Digite o valor:");
    float porcentagem = Float.parseFloat(sc.nextLine());

    int res = ctrl.AlterarPorcentagem(porcentagem);
    
    if(res==1){
        System.out.println("Porcentagem alterada com sucesso!");
    }else{
        System.out.println("Falha na alteração da porcentagem.");
    }
   
}

private void ExcluirPacote(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    int res = ctrl.ExcluirPacote(codigo);
    
    if(res==1){
        System.out.println("Pacote excluido com sucesso!");
    }else{
        System.out.println("Falha na exclusão do pacote.");
    }
    
}

public void menu3(){        
        int opcao;
        
        System.out.println("Gerenciar Passeios");
        
        do{
            System.out.println("1 - Incluir");
            System.out.println("2 - Alterar");
            System.out.println("3 - Excluir");
            System.out.println("4 - Sair");
            opcao = Integer.parseInt(sc.nextLine());
            switch(opcao){
                case 1:
                   at.IncluirPasseio();
                break;
                case 2:
                    at.AlterarPasseio();
                break;
                case 3:
                    at.ExcluirPasseio();
                break;
            }
        }while(opcao != 4);
        
    }

private void IncluirPasseio(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();

    System.out.println("Preço:");
    float preco = Float.parseFloat(sc.nextLine());

    System.out.println("Cidade:");
    String cidadePasseio = sc.nextLine();
    
    System.out.println("Lugar:");
    String lugar = sc.nextLine();
    
    String tipo="";
    System.out.println("Tipo(Ar livre ou Local fechado):");
    while(tipo.equalsIgnoreCase("Ar livre")==false && tipo.equalsIgnoreCase("Local fechado")==false){
        tipo = sc.nextLine();
    }
    
    System.out.println("Horário de Início:");
    int hInicio=-1,mInicio=-1;
    System.out.print("Hora(hh): ");
    while(hInicio<0 || hInicio>23){
        hInicio = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Minuto(mm): ");
    while(mInicio<0 || mInicio>59){
        mInicio = Integer.parseInt(sc.nextLine());
    }
    
    String horainicio,mininicio;
    horainicio = String.valueOf(hInicio);
    mininicio = String.valueOf(mInicio);
    
    if(hInicio<10){
        horainicio = "0" + String.valueOf(hInicio);
    }
    if(mInicio<10){
        mininicio = "0" + String.valueOf(mInicio);
    }
    
    String horaInicio = horainicio+":"+mininicio;
    
    System.out.println("Horário de Término:");
    int hFim=-1,mFim=-1;
    System.out.print("Hora(hh): ");
    while(hFim<0 || hFim>23){
        hFim = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Minuto(mm): ");
    while(mFim<0 || mFim>59){
        mFim = Integer.parseInt(sc.nextLine());
    }
    
    String horafim,minfim;
    horafim = String.valueOf(hFim);
    minfim = String.valueOf(mFim);
    
    if(hFim<10){
        horafim = "0" + String.valueOf(hFim);
    }
    if(mInicio<10){
        minfim = "0" + String.valueOf(mFim);
    }
    
    String horaFim = horafim+":"+minfim;
    
    int res = ctrl.IncluirPasseio(codigo, preco, cidadePasseio, lugar, tipo, horaInicio, horaFim);
    
    if(res==1){
        System.out.println("Passeio incluído com sucesso!");
    }else{
        System.out.println("Falha no cadastro do passeio.");
    }
   
}

private void AlterarPasseio(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();

    System.out.println("Novo preço:");
    float preco = Float.parseFloat(sc.nextLine());

    System.out.println("Nova cidade:");
    String cidadePasseio = sc.nextLine();
    
    System.out.println("Novo lugar:");
    String lugar = sc.nextLine();
    
    String tipo="";
    System.out.println("Novo tipo(Ar livre ou Local fechado):");
    while(tipo.equalsIgnoreCase("Ar livre")==false && tipo.equalsIgnoreCase("Local fechado")==false){
        tipo = sc.nextLine();
    }
    
    System.out.println("Horário de Início:");
    int hInicio=-1,mInicio=-1;
    System.out.print("Nova hora(hh): ");
    while(hInicio<0 || hInicio>23){
        hInicio = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Novo minuto(mm): ");
    while(mInicio<0 || mInicio>59){
        mInicio = Integer.parseInt(sc.nextLine());
    }

    String horainicio,mininicio;
    horainicio = String.valueOf(hInicio);
    mininicio = String.valueOf(mInicio);
    
    if(hInicio<10){
        horainicio = "0" + String.valueOf(hInicio);
    }
    if(mInicio<10){
        mininicio = "0" + String.valueOf(mInicio);
    }
    
    String horaInicio = horainicio+":"+mininicio;
    
    System.out.println("Horário de Término:");
    int hFim=-1,mFim=-1;
    System.out.print("Nova hora(hh): ");
    while(hFim<0 || hFim>23){
        hFim = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Novo minuto(mm): ");
    while(mFim<0 || mFim>59){
        mFim = Integer.parseInt(sc.nextLine());
    }
    
    String horafim,minfim;
    horafim = String.valueOf(hFim);
    minfim = String.valueOf(mFim);
    
    if(hFim<10){
        horafim = "0" + String.valueOf(hFim);
    }
    if(mInicio<10){
        minfim = "0" + String.valueOf(mFim);
    }
    
    String horaFim = horafim +":"+ minfim;
    
    int res = ctrl.AlterarPasseio(codigo, preco, cidadePasseio, lugar, tipo, horaInicio, horaFim);
    
    if(res==1){
        System.out.println("Passeio alterado com sucesso!");
    }else{
        System.out.println("Falha na alteração do passeio.");
    }
    
}

private void ExcluirPasseio(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    int res = ctrl.ExcluirPacote(codigo);
    
    if(res==1){
        System.out.println("Passeio excluido com sucesso!");
    }else{
        System.out.println("Falha na exclusão do passeio.");
    }
    
}

public void menu4(){      
        int opcao;
        
        System.out.println("Gerenciar Viagens");
        
        do{
            System.out.println("1 - Incluir");
            System.out.println("2 - Alterar");
            System.out.println("3 - Excluir");
            System.out.println("4 - Sair");
            opcao = Integer.parseInt(sc.nextLine());
            switch(opcao){
                case 1:
                   at.IncluirViagem();
                break;
                case 2:
                    at.AlterarViagem();
                break;
                case 3:
                    at.ExcluirViagem();
                break;
            }
        }while(opcao != 4);
        
    }

private void IncluirViagem(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    System.out.println("Data de Início:");
    System.out.print("Dia: ");
    int dia=0,mes=0,ano=0;
    while(dia<1 || dia>31){
        dia = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Mês: ");
    while(mes<1 || mes>12){
        mes = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Ano: ");
    while(String.valueOf(ano).length()!=4){
        ano = Integer.parseInt(sc.nextLine());
    }
    
    String diastring,messtring,anostring;
    diastring = String.valueOf(dia);
    messtring = String.valueOf(mes);
    anostring = String.valueOf(ano);
    
    if(dia<10){
        diastring = "0" + String.valueOf(dia);
    }
    if(mes<10){
        messtring = "0" + String.valueOf(mes);
    }
    if(ano<10){
        messtring = "000" + String.valueOf(ano);
    }
    if(ano<100){
        messtring = "00" + String.valueOf(ano);
    }
    if(ano<1000){
        messtring = "0" + String.valueOf(ano);
    }
    
    String dataInicio = diastring + "/" + messtring + "/" + anostring;
    
    System.out.println("Pacote:");
    System.out.print("Código: ");
    String codigoPacote = sc.nextLine();
    
    int a=1;
    ArrayList<String> codigosPasseios = new ArrayList<>();
    while(a==1){
        System.out.println("Adicionar passeio(Deixe em branco para nenhum):");
        System.out.print("Código: ");
        String codigoPasseio = sc.nextLine();
        if(codigoPasseio.equals("")){
            a=0;
        }else{
            codigosPasseios.add(codigoPasseio);
        }
    }
    
    int res = ctrl.IncluirViagem(codigo, dataInicio, codigoPacote, codigosPasseios);
    
    if(res==1){
        System.out.println("Viagem incluída com sucesso!");
    }else{
        System.out.println("Falha no cadastro da viagem.");
    }
   
}

private void AlterarViagem(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    System.out.println("Data de Início:");
    System.out.print("Novo dia: ");
    int dia=0,mes=0,ano=0;
    while(dia<1 || dia>31){
        dia = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Novo mês: ");
    while(mes<1 || mes>12){
        mes = Integer.parseInt(sc.nextLine());
    }
    System.out.print("Novo ano: ");
    while(String.valueOf(ano).length()!=4){
        ano = Integer.parseInt(sc.nextLine());
    }
    
    String diastring,messtring,anostring;
    diastring = String.valueOf(dia);
    messtring = String.valueOf(mes);
    anostring = String.valueOf(ano);
    
    if(dia<10){
        diastring = "0" + String.valueOf(dia);
    }
    if(mes<10){
        messtring = "0" + String.valueOf(mes);
    }
    if(ano<10){
        messtring = "000" + String.valueOf(ano);
    }
    if(ano<100){
        messtring = "00" + String.valueOf(ano);
    }
    if(ano<1000){
        messtring = "0" + String.valueOf(ano);
    }
    
    String dataInicio = diastring + "/" + messtring + "/" + anostring;
    
    System.out.println("Pacote:");
    System.out.print("Novo código: ");
    String codigoPacote = sc.nextLine();
    
    int a=1;
    ArrayList<String> codigosPasseios = new ArrayList<>();
    while(a==1){
        System.out.println("Adicionar passeio(Deixe em branco para nenhum):");
        System.out.print("Novo código: ");
        String codigoPasseio = sc.nextLine();
        if(codigoPasseio.equals("")){
            a=0;
        }else{
            codigosPasseios.add(codigoPasseio);
        }
    }
    
    int res = ctrl.AlterarViagem(codigo, dataInicio, codigoPacote, codigosPasseios);
    
    if(res==1){
        System.out.println("Viagem alterada com sucesso!");
    }else{
        System.out.println("Falha na alteração da viagem.");
    }
   
}

private void ExcluirViagem(){
    
    System.out.println("Código:");
    String codigo = sc.nextLine();
    
    int res = ctrl.ExcluirViagem(codigo);
    
    if(res==1){
        System.out.println("Viagem excluída com sucesso!");
    }else{
        System.out.println("Falha na exclusão da viagem.");
    }
    
}

public void menu5(){
        int opcao;
        
        System.out.println("Visualizar Relatório");
        
        do{
            System.out.println("1 - Mensal");
            System.out.println("2 - Anual");
            System.out.println("3 - Sair");
            opcao = Integer.parseInt(sc.nextLine());
            switch(opcao){
                case 1:
                   at.relatorioMensal();
                break;
                case 2:
                    at.relatorioAnual();
                break;
            }
        }while(opcao != 3);
        
    }


private void relatorioMensal(){
    int mes=0;
    System.out.print("Mês: ");
    
    while(mes<1 || mes>12){
        mes = Integer.parseInt(sc.nextLine());
    }
    
    ctrl.relatorioMensal(mes);
    
}

private void relatorioAnual(){
    int ano=-1;
    System.out.print("Ano: ");
    while(ano<0){
        ano = Integer.parseInt(sc.nextLine());
    }
    
    ctrl.relatorioAnual(ano);
    
}

}